//John Maeda - Morisawa Posters - Mori No.6 

PFont font;
int posY = 190;


void setup() {
	size(800, 1130);
	font = createFont("NicoKaku", 310, true);
	// font = loadFont("HiraKakuStdN-W8-48.vlw");
	textFont(font);
	frameRate(5);
	
}

void draw() {
	background(240);
	fill(0);
	//textAlign(CENTER, CENTER);
	text("モ", -15, posY); // MO
	text("リ", 200, posY); // RI
	text("サ", 360, posY); // SA
	text("ワ", 570, posY); // WA

	posY = posY+ 200;
	noFill();
	stroke(0);
	
}