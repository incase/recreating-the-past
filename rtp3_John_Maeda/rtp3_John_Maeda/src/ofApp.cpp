#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofTrueTypeFontSettings settings("nicokaku_v1.ttf", 264);
    settings.antialiased = true;
    settings.dpi = 72;
    settings.direction = OF_TTF_LEFT_TO_RIGHT;
    settings.contours = true;
    //settings.addRanges(ofAlphabet::Japanese);
    settings.addRange(ofUnicode::Katakana);
    font.load(settings);
//    font.setLetterSpacing(0.10);
//    font.setSpaceSize(100.0);
}

//--------------------------------------------------------------
void ofApp::update(){
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    float xBegin = 0.0;
    float yBegin = 0.0;
    float width = ofGetWidth();
    float height = ofGetHeight();
    int repeatLetters = 350;
    
    ofBackground(245);
    ofSetColor(0);

    string logo = "モリサワ"; //MO-RI-SA-WA
    ofRectangle bounds = font.getStringBoundingBox(logo, 0.0, 0.0);
//    ofDrawRectangle(bounds);
    
    xBegin = (width-bounds.width)/2-10;
    yBegin = (bounds.height+3);
//    std::cout << "yBegin: " << yBegin << endl;
    vector < ofPath > paths = font.getStringAsPoints(logo, true, false);
    
//    std::cout << "paths: " << paths.size() << endl;
    
    ofNoFill();
    ofSetLineWidth(1);
    
    ofPushMatrix();
    ofTranslate(xBegin,yBegin);
    int character = paths.size();
    for (int i = 0; i < character; i++){
        ofPath path = paths[i];
        vector < ofPolyline > lines = path.getOutline();
        float xPos = 0.0;
        float freq = 0.039; //mouseX * 0.001;
        float amp = 3.87; //mouseX * 0.01;
        float noiseFactor = 0.009;
//        float noiseFactorTwo = mouseX * 0.01;
        float rot = 0.299; //mouseX * 0.001;
//        std::cout << "nf2: " << noiseFactorTwo << endl;
        ofPushMatrix();
        for(int j = 0; j < repeatLetters; j++ ){
            float yPos = (j+1)/9.2;
            
            if(i >= character - 2){
                xPos = amp * sin(-j * freq) * ofNoise(j* noiseFactor, i * 1.39);
                //ofSetColor(255,0,0);
//              std::cout << "i: " << i << endl;
            } else{
                xPos = amp * sin(j * freq) * ofNoise(j* noiseFactor, i * 0.047);
            }
            
//            std::cout << "yPos: " << yPos << endl;
            ofTranslate(xPos, yPos);
            ofRotateZDeg(ofSignedNoise(rot, i));
            
            
            for (int k = 0; k < lines.size(); k++){
                ofPolyline line = lines[k];
                ofSetColor(80, 80, 80, ofMap(yPos, 0, 16 , 255, 0));
                lines[k].draw();
            }
        
        }
        ofPopMatrix();
        
    }
    
    
    ofSetColor(0);
    font.drawString(logo, 0, 0); // MO - RI - SA - WA
    ofPopMatrix();
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
