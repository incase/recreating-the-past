// Original Artwork Vera Molnár - 25 carrés (1989)
// Approximate recreation 2019 - Karsten Schuhl

int bgSize = 1000;

int squareSize = bgSize/6;
int margin = squareSize;
float noiseScale = 1.4585; 
int offAmount = 15;

color k = color(0, 0, 0); 
color bg = color(230, 230, 230);
int squareAlpha = 80;

void setup(){
	size(1000, 1000);
	noiseSeed(0);
	randomSeed(1);
	colorMode(RGB, 255, 255, 255, 100);
	noStroke();
	//frameRate(5);
	noLoop();
}

void draw(){
	background(bg);
	rectMode(CENTER);
	fill(k, squareAlpha);
	
	pushMatrix();
	translate(0, bgSize);
	scale(1, -1);

	for (int i=0; i < 5; i++){
		for (int j=0; j < 5; j++){
		 	//dimX = map(mouseX, 0, 1000, 0.01, 0.9);
			//dimY = map(mouseY, 0, 1000, 0.01, 0.9);
			float posX = (map(i, 0, 4, squareSize, (bgSize-squareSize)));
			float posY = (map(j, 0, 4, squareSize, (bgSize-squareSize))*noise(i*0.02, j*0.04))*noiseScale;
			float offset = random(-offAmount, offAmount);	
			pushMatrix();
			translate(posX,posY);
			rect(0+offset , 0+offset, squareSize, squareSize);
			popMatrix();	
		}
	}
	popMatrix();
}