// custom ControlBehavior class
// not yet adjusted to be used for general purpose animation of control p5 Sliders
// needs adjustment of high and low point to oscillate between

class SliderCos extends ControlBehavior {
  int id;

  SliderCos(int sliderId) {
    id = sliderId;
  }

  public void update() {

    switch(switchCount) {
    case 0:
      /*print("switchCase = " + switchCount);
       print(" | Behaviour is active = " + behaviour);
       print(" | a = " + a);
       println(" | value = " + getValue());*/
      setValue(-cos(period[id] += factor[id]) * (round(sliderRangeLow[id]/2))  + (round(sliderRangeHigh[id]/2))); 
      break;
    case 1:

      while (doSave == 0) {
        pastSliderValue[id] = getValue();
        doSave = 1;
        print("switchCount = " + switchCount);
        println(" | pastSliderValue = " + pastSliderValue[id]);
      }
      break;
    case 2:
      doSave = 0;
      println("switchCount = " + switchCount);
      currentSliderValue[id] = myListeners[id].col; 
      if (currentSliderValue[id] != int(pastSliderValue[id])) {
        println("go to zero from new position!");
        if (currentSliderValue[id] > 0) {
          setValue(currentSliderValue[id] -= 0.0001);
        }
        period[id] = 0;
        if (currentSliderValue[id] == 0) {
          switchCount = 0;
        }
      } else {
        println("go on with it from before!");
        switchCount = 0;
      }
      break;
    }
  }
}