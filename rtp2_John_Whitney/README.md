# John Whitney Sr. - Matrix III

**Original work - Screenshot**

<img src="../Images/rtp2_John_Whitney_MatrixIII.png" width="500" /> 

**Adaption - Hexagons**

<img src="../Images/rtp2_John_Whitney_MatrixIII_Hexagons.png" width="500" />



**Matrix III - Youtube**
[![](http://img.youtube.com/vi/ZrKgyY5aDvA/0.jpg)](http://www.youtube.com/watch?v=ZrKgyY5aDvA "Video - John Whitney - Matrix III")

**Video example**
[![](http://img.youtube.com/vi/BrDilUAzJlA/0.jpg)](http://www.youtube.com/watch?v=BrDilUAzJlA "Video Hexagon Adaption")