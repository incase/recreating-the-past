import controlP5.*;

ControlP5 cp5;
MyControlListener[] myListeners;
SliderCos[] sliderCos;

float a = 0;
boolean behaviour = false;
int switchCount = 0;
int doSave = 0;

int numSliders = 5;
float[] factor = new float[numSliders];
float[] period = new float[numSliders];

float[] currentSliderValue = new float[numSliders];
float[] pastSliderValue = new float[numSliders];

float [] sliderRangeLow = { -10.0, -1.0, -1.0, 0.1, 0.1 };
float [] sliderRangeHigh = { 10.0, 1.0, 1.0, 8.0, 1.0 };


float bgAlpha = 50;
float xOrig = 500;
float yOrig = 500;
float lissRad = 250;
float time = 0.0;

float jStretch = 10;
float xScale = 0.5;
float yScale = 0.5;
float hexScale = 1;
float hexPer = 0.3;

color pink = color(255, 200, 220);
color darkPink = color(155, 100, 120);
color grey = color(127, 127, 127);

void setup() {
  size(1000, 1000);
  colorMode(RGB, 255, 255, 255, 100);
  noFill();
  cp5 = new ControlP5(this);

  // Control

  cp5.addSlider("jStretch")
     .setPosition(20,40)
     .setRange(-10,10)
     .setColorActive(color(grey))
     .setColorForeground(color(pink))
     .setColorBackground(color(darkPink))
     ;
  cp5.addSlider("xScale")
     .setPosition(20,60)
     .setRange(-1,1)
     .setColorActive(color(grey))
     .setColorForeground(color(pink))
     .setColorBackground(color(darkPink))
     ;
  cp5.addSlider("yScale")
     .setPosition(20,80)
     .setRange(-1,1)
     .setColorActive(color(grey))
     .setColorForeground(color(pink))
     .setColorBackground(color(darkPink))
     ;      
  cp5.addSlider("hexScale")
     .setPosition(20,100)
     .setRange(0.1,8.0)
     .setColorActive(color(grey))
     .setColorForeground(color(pink))
     .setColorBackground(color(darkPink))
     ;      
  cp5.addSlider("hexPer")
     .setPosition(20,120)
     .setRange(0.1,1.0)
     .setColorActive(color(grey))
     .setColorForeground(color(pink))
     .setColorBackground(color(darkPink))
     ;


/* deactivated auto-animation of sliders for now because of range issues

  myListeners = new MyControlListener[numSliders];
  sliderCos = new SliderCos[numSliders];

  for (int i = 0; i < numSliders; i++) {
    myListeners[i] = new MyControlListener();
    sliderCos[i] = new SliderCos(i);
    currentSliderValue[i] = 0;
    pastSliderValue[i] = 0;
  }
  
  float[] factor = { 0.001, 0.001, 0.001, 0.001, 0.001 };

  cp5.getController("jStretch").setBehavior(sliderCos[0]);
  cp5.getController("jStretch").addListener(myListeners[0]);
 
  cp5.getController("xScale").setBehavior(sliderCos[1]); 
  cp5.getController("xScale").addListener(myListeners[1]);

  cp5.getController("yScale").setBehavior(sliderCos[2]); 
  cp5.getController("yScale").addListener(myListeners[2]);

  cp5.getController("hexScale").setBehavior(sliderCos[3]); 
  cp5.getController("hexScale").addListener(myListeners[3]);

  cp5.getController("hexPer").setBehavior(sliderCos[4]); 
  cp5.getController("hexPer").addListener(myListeners[4]);

  behaviour = false;
  */

}



void draw() {

  background(0);
  // fill(0, 30);
  // rect(-10, -10, width+10, height+10);
  // noFill();
  // 
  float time = millis()/1000.0;
  float angle = time;
  
  stroke(255, 200, 220, 100);
  strokeWeight(3);
  //float hexRad = 5;

  for (int i = 0; i < 5; i++){
    
    for(int j = 0; j < 8; j++){
      
      // float xScale = map(sin(time), -1, 1, 0, 0.5);
      // float yScale = map(cos(time), -1, 1, 0, 0.5);
      
      
      float x = xOrig + lissRad * cos(((i*xScale)+angle+(j*jStretch))*0.432);
      float y = yOrig + lissRad * sin(((i*yScale)+angle+(j*jStretch))*0.287);

      // float c = int(map(j, 0, 7, 100, 255));
      // stroke(c);    
      float hexRad = map(sin((angle+i)*hexPer), 1, -1, 3, 120)*hexScale;

      drawHexagon(x, y, hexRad);  
    }
  }
  
  for (int i = 0; i < numSliders; i++) {
    //println("factor " + i + ": " + factor[i]);
    if (factor[i] >= TWO_PI) {
      factor[i] = 0;
    }
  }
}


void drawHexagon(float x, float y, float radius) {
  pushMatrix();
  translate(x, y);
  rotate(PI/6.0);
  beginShape();
  for (int i = 0; i < 6; i++) {
    pushMatrix();
    float theta = PI*i/3;
    vertex(cos(theta) * radius, sin(theta) * radius);
    popMatrix();
  }
  endShape(CLOSE);
  popMatrix();
}

void keyReleased() {
  if (key == 'b' || key == 'B') {
    behaviour = !behaviour;
    switchCount ++;
    if (switchCount > 2) {
      switchCount = 0;
    }
    print(" *** behaviour should be: " + behaviour);
    print(" | a = " + a);
    println(" | switchCount = " + switchCount + "*** ");
  }
}

