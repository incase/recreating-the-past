# Ken Knowlton

**Inspiration Mosaic Portrait of Joseph Scala**

<img src="../Images/rtp4_Ken_Knowlton_Scala.jpg" width="500" /> 

**Triangles based on pixel brightness**

<img src="../Images/rtp4_Ken_Knowlton_MosaicTriangles.png" width="500" />