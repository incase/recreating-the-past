#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    grabber.setup(640, 480);
}

//--------------------------------------------------------------
void ofApp::update(){
    grabber.update();
}

//--------------------------------------------------------------
void drawTriangleSingles(float brightness){
    if(brightness <= 63){
        ofDrawTriangle(0, 0, 5, 5, 0, 10);
    } else if (64 <= brightness && brightness <= 128){
        ofDrawTriangle(0, 0, 5, 5, 10, 0);
    } else if (129 <= brightness && brightness <= 192){
        ofDrawTriangle(10, 0, 5, 5, 10, 10);
    } else if (brightness > 192) {
        ofDrawTriangle(0, 10, 5, 5, 10, 10);
    }
}

//--------------------------------------------------------------
void drawTriangle(float brightness){
    ofFill();
    ofSetColor(230);
    if(brightness <= 63){
        ofDrawTriangle(0, 0, 5, 5, 0, 10);
    } else if (64 <= brightness && brightness <= 128){
        ofDrawTriangle(0, 0, 10, 0, 10, 0);
    } else if (129 <= brightness && brightness <= 192){
        ofDrawTriangle(0, 0, 10, 0, 10, 0);
        ofDrawTriangle(10, 0, 5, 5, 10, 10);
    } else if (brightness > 192) {
        ofDrawRectangle(0, 0, 10, 10);
    }
}

//--------------------------------------------------------------
void drawTriangleStroked(float brightness){
    
    ofFill();
    ofSetColor(230);
    if(brightness <= 63){
        ofDrawTriangle(0, 0, 5, 5, 0, 10);
    } else if (64 <= brightness && brightness <= 128){
        ofDrawTriangle(0, 0, 10, 0, 10, 0);
    } else if (129 <= brightness && brightness <= 192){
        ofBeginShape();
          ofVertex(0,0);
          ofVertex(0,10);
          ofVertex(10,10);
          ofVertex(5,5);
          ofVertex(0,10);
        ofEndShape();
    } else if (brightness > 192) {
        ofDrawRectangle(0, 0, 10, 10);
    }

    ofNoFill();
    ofSetColor(65);
    ofSetLineWidth(ofMap(brightness, 0, 255, 0, 4));

    if(brightness <= 63){
        ofDrawTriangle(0, 0, 5, 5, 0, 10);
    } else if (64 <= brightness && brightness <= 128){
        ofDrawTriangle(0, 0, 10, 0, 10, 0);
    } else if (129 <= brightness && brightness <= 192){
        ofBeginShape();
          ofVertex(0,0);
          ofVertex(0,10);
          ofVertex(10,10);
          ofVertex(5,5);
          ofVertex(0,10);
        ofEndShape();
    } else if (brightness > 192) {
        ofDrawRectangle(0, 0, 10, 10);
    }
}


//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(65);
    ofSetColor(230);
    
    for (int i = 0; i < grabber.getWidth(); i+=10){
          for (int j = 0; j < grabber.getHeight(); j+=10){
              ofColor color = grabber.getPixels().getColor(i,j);  //DISNEYLAND
              float brightness = color.getBrightness();
              //ofSetColor(color);
              ofPushMatrix();
              ofTranslate(i, j);
              if (click == false){
                  drawTriangle(brightness);
              } else {
                  drawTriangleStroked(brightness);
              }
              ofPopMatrix();
              
          }
      }

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    if (click == false){
        click = true;
    } else if(click == true){
        click = false;
    }

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
